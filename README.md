# Test the project

Run `g++ -std=c++11 tests.cpp` and `./a.out` command.

# Simulation results

Run `g++ -std=c++11 simulate.cpp` and `./a.out` command.
